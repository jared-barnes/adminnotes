BASE.require([
    "BASE.data.Edm",
    "BASE.data.ModelGenerator",
    "Enum"
], function() {

    BASE.namespace("app.model");

    var PriorityType = function(){};
    PriorityType.Low = new Enum(0);
    PriorityType.Low.name = "Low";

    PriorityType.Medium = new Enum(1);
    PriorityType.Medium.name = "Medium";

    PriorityType.High = new Enum(2);
    PriorityType.High.name = "High";

    var Edm = BASE.data.Edm;

    var config = {
        models: [{
            "@type": "app.model.Person",
            "collectionName": "people",
            "displayInstance": function(entity) {
                if (entity == null) {
                    return "";
                }
                return entity.firstName + " " + entity.lastName;
            },
            "labelInstance": function() {
                return "Person";
            },
            "properties": {
                "id": {
                    "@type": "Integer",
                    "primaryKey": true,
                    "autoIncremement": true
                },
                "firstName": {
                    "@type": "String"
                },
                "lastName": {
                    "@type": "String"
                },
                "email": {
                    "@type": "String"
                },
                "phoneNumber": {
                    "@type": "String"
                },
            }
        }, {
            "@type": "app.model.Note",
            "collectionName": "notes",
            "displayInstance": function(entity) {
                return entity.message;
            },
            "labelInstance": function() {
                return "Note";
            },
            "properties": {
                "id": {
                    "@type": "Integer",
                    "primaryKey": true,
                    "autoIncremement": true
                },
                "personId": {
                    "@type": "Integer"
                },
                "date":{
                  "@type":"DateTimeOffset"
                },
                "message": {
                    "@type": "String",
                    "length": 1000
                }
            }
        },
        {
            "@type": "app.model.Task",
            "collectionName": "tasks",
            "displayInstance": function(entity) {
                return entity.name;
            },
            "labelInstance": function() {
                return "Task";
            },
            "properties": {
                "id": {
                    "@type": "Integer",
                    "primaryKey": true,
                    "autoIncremement": true
                },
                "isComplete": {
                    "@type": "Boolean",
                    "defaultValue": false
                },
                "priority": {
                    "@type": "Enum",
                    "genericTypeParameters":[PriorityType]
                },
                "completeBy":{
                  "@type": "DateTimeOffset"
                },
                "action":{
                  "@type":"String",
                  "length": 1000
                },
                "personId":{
                  "@type":"Integer"
                }

            }
        }],
        relationships: {
            oneToOne: [],
            oneToMany: [{
                "@type": "app.model.Person",
                "hasKey": "id",
                "hasMany": "notes",
                "@ofType": "app.model.Note",
                "withKey": "id",
                "withForeignKey": "personId",
                "withOne": "person"
            },{
                "@type": "app.model.Person",
                "hasKey": "id",
                "hasMany": "tasks",
                "@ofType": "app.model.Task",
                "withKey": "id",
                "withForeignKey": "personId",
                "withOne": "person"
            }],
            manyToMany: []
        }

    };

    app.model.Edm = function() {
        Edm.call(this);
        var modelGenerator = new BASE.data.ModelGenerator(config);
        modelGenerator.addToEdm(this);
    };

    BASE.extend(app.model.Edm, Edm);

});
