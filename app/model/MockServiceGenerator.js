﻿BASE.require([
    "BASE.data.services.InMemoryService",
    "BASE.sqlite.Service",
    "BASE.data.services.IndexedDbService"
], function () {

    BASE.namespace("app.model");

    var InMemoryService = BASE.data.services.InMemoryService;
    var SqliteService = BASE.sqlite.Service;

    app.model.MockServiceGenerator = {
        "sqlite": function (name, edm) {
            name = name || "Unknown";
            var service = new SqliteService({
                edm: edm,
                name: name
            });

            service.initializeAsync().try();
            return service;
        },
        "inMemory": function (name, edm) {
            return new InMemoryService(edm);
        },
        "indexedDb": function (name, edm) {
            name = name || "Unknown";
            return new IndexedDb({
                name: name,
                edm: edm
            });
        }
    };


});
