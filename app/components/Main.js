BASE.require([
    "jQuery",
    "components.gem.DefaultDisplayService",
    "BASE.data.CsvGenerator"
], function() {

    BASE.namespace("app.components");

    var DefaultDisplayService = components.gem.DefaultDisplayService;
    var CsvGenerator = BASE.data.CsvGenerator;

    var CsvNote = function() {
        this.person = null;
        this.project = null;
        this.message = null;
    };

    app.components.Main = function(elem, tags, services) {
        var self = this;
        var $elem = $(elem);
        var databaseManager = $(tags["database-manager"]).controller();
        var dataService = services.get("dataService");
        var defaultDisplayService = new DefaultDisplayService(dataService);

        // Note Display
        var noteTypeDisplay = defaultDisplayService.getDisplayByType(app.model.Note);
        noteTypeDisplay.configureInputByName("message", {
            span: 12,
        });
        noteTypeDisplay.configureInputByName("date", {
            span: 12,
        });
        noteTypeDisplay.configureInputByName("person", {
            span: 12,
        });
        noteTypeDisplay.configureListPropertyByName("message", {
            width: 400
        });
        noteTypeDisplay.listProperties.push({
            name: "person",
            propertyName: "person",
            width: 200,
            label: function() {
                return "Person";
            },
            getValue: function(entity) {
                return entity.person;
            },
            display: function(person) {
                return person.firstName + " " + person.lastName;
            },
            search: function(queryable, tokenText, orderByAsc, orderByDesc) {
                var token = tokenText.trim();

                if (token) {
                    queryable = queryable.or(function(expBuilder) {
                        return expBuilder.or(
                            expBuilder.property("person").property("firstName").contains(token),
                            expBuilder.property("person").property("lastName").contains(token)
                        );
                    });
                }

                if (orderByAsc) {
                    queryable = queryable.orderBy(function(expBuilder) {
                        return expBuilder.property("person").property("firstName");
                    });
                } else if (orderByDesc) {
                    queryable = queryable.orderByDesc(function(expBuilder) {
                        return expBuilder.property("person").property("firstName");
                    });
                }

                queryable = queryable.include(function(expBuilder) {
                    return expBuilder.property("person");
                });

                return queryable;

            },
        });

        noteTypeDisplay.windowSize = {
            width: 400,
            height: 550
        };
        noteTypeDisplay.setSortOrderByNames(["person", "date", "message"]);
        noteTypeDisplay.actions.push({
            isSeperator: true
        }, {
            label: function() {
                return "Export"
            },
            isActionable: function() {
                return true;
            },
            callback: function() {
                var csvGenerator = new CsvGenerator(CsvNote);

                dataService.asQueryable(app.model.Note).include(function(expBuilder) {
                    return expBuilder.property("person");
                }).toArray(function(array) {
                    var results = array.map(function(entity) {
                        var note = new CsvNote();
                        note.person = entity.person.firstName + " " + entity.person.lastName;
                        note.project = entity.project.name;
                        note.message = entity.message;
                        return note;
                    });

                    var blob = csvGenerator.toCsvBlob(results);
                    var reader = new FileReader();

                    reader.onload = function() {
                        window.location.href = reader.result;
                    };
                    reader.readAsDataURL(blob);
                });
            }
        });

        // Project Display
        var taskTypeDisplay = defaultDisplayService.getDisplayByType(app.model.Task);
        taskTypeDisplay.configureInputByName("person", {
            span: 12
        });
        taskTypeDisplay.configureInputByName("priority", {
            span: 12
        });
        taskTypeDisplay.configureInputByName("completeBy", {
            span: 12
        });
        taskTypeDisplay.configureInputByName("isComplete", {
            span: 12
        });
        taskTypeDisplay.configureInputByName("action", {
            span: 12
        });
        taskTypeDisplay.windowSize = {
            width: 400,
            height: 700
        };
        taskTypeDisplay.setSortOrderByNames([
            "person",
            "isComplete",
            "priority",
            "completeBy",
            "action"
        ]);

        databaseManager.setDisplayServiceAsync(defaultDisplayService, {
            rootTypes: [
                app.model.Person,
                app.model.Note,
                app.model.Task
            ]
        }).try();
    };

});
